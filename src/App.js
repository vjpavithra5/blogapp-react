import './App.css';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import BlogList from './comps/BlogList'
import BlogPage from './comps/BlogPage'
import Header from './comps/Header';
import CreateBlog from './comps/CreateBlog';
import EditBlog from './comps/EditBlog';

const App = () => {
  return (
      <Router>
        <div>
          <Header />
            <Route path={["/","/blogs" ]} exact> 
              <BlogList />
            </Route>
            <Route exact path="/blogs/create">
              <CreateBlog />
            </Route>
            <Route exact path="/blog/:id">
              <BlogPage />
            </Route>
            <Route exact path="/blog/:id/edit">
              <EditBlog />
            </Route>
        </div>
      </Router>
  );
}

export default App;
