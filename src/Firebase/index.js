import firebase from 'firebase/compat/app'
import 'firebase/compat/storage';

const firebaseConfig = {
    apiKey: "AIzaSyC-EdYPkk7STK454Tvr8VpU0SLZxa47euM",
    authDomain: "turnkey-highway-298207.firebaseapp.com",
    projectId: "turnkey-highway-298207",
    storageBucket: "turnkey-highway-298207.appspot.com",
    messagingSenderId: "534002137902",
    appId: "1:534002137902:web:2b2ee77f54a91fa73bd8bf",
    measurementId: "G-XCC3K4GHBL"
  };
  
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
const storage = firebase.storage()
export default storage