import axios from 'axios';
import React,{useState} from 'react'
import { baseUrl } from '../Base';
import { useHistory } from 'react-router-dom';
import storage from '../Firebase/index'


const CreateBlog = () => {
    let history = useHistory()

    let initialBlog = {
        title: "",
        author:"",
        imgUrl:"",
        article:""
    }

    const [ newBlog, setNewBlog] = useState(initialBlog);
    const [progress, setProgress] = useState(0)

    let finalProgress = progress + '%';
    let btnClass = `btn btn-primary ${progress == 100 ? '' : 'disabled' }`

    let handleChange = (e) => {
        const {name, value} = e.target;
        setNewBlog({...newBlog,[name]:value})
    }

    let createBlog = (e) =>{
        e.preventDefault()
        let data = newBlog;
        console.log(data)
        axios.post(`${baseUrl}/api/blogs/create`, data)
            .then(response => {history.push('/blogs/')})
    }

    let handleImage = (e) => {
        let img = e.target.files[0]
        const uploadTask = storage.ref(`images/${img.name}`).put(img);
        uploadTask.on(
            "state_changed",
            snapshot => {
                let prog = Math.round(
                  (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );
                setProgress(prog)
              },
            error => {
              console.log(error);
            },
            () => {
              storage
                .ref("images")
                .child(img.name)
                .getDownloadURL()
                .then(url => {
                    setNewBlog(prevState =>{ return {...prevState, imgUrl:url}})
                });
            }
          );
    }

    return (
        <div className="mb-5 container">
            <h2 className="text-center">Create a New Post</h2>
            <form >
                <div className="mb-3">
                    <label htmlFor="title" className="form-label">Title</label>
                    <input type="text" className="form-control" name="title" id="title" aria-describedby="title" 
                    onChange={handleChange} required/>
                </div>
                <div className="mb-3">
                    <label htmlFor="image" className="form-label">Upload Image</label>
                    <input type="file" className="form-control" name="imgUrl" id="image" aria-describedby="image" 
                    onChange={handleImage} required/>
                    { progress > 0 && <div className="progress mt-4" style={{height: '3px'}}>
                        <div className="progress-bar bg-success" role="progressbar" style={{width: finalProgress }}
                        aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100"></div>
                    </div> }
                </div>
                <div className="mb-3">
                    <label htmlFor="author" className="form-label">Author </label>
                    <input type="text" className="form-control" name="author" id="author" aria-describedby="author" 
                    onChange={handleChange} required/>
                </div>
                <div className="mb-3">
                    <label htmlFor="article" className="form-label">Post</label>
                    <textarea className="form-control" name="article" aria-label="article" onChange={handleChange}
                    rows='15'></textarea>
                </div>
                <button type="button" className={btnClass} onClick={createBlog}>Submit</button>
            </form>
        </div>
    )
}

export default CreateBlog
