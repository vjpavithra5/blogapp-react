import axios from 'axios';
import React,{useState, useEffect} from 'react'
import { baseUrl } from '../Base';
import { useHistory } from 'react-router-dom';
import storage from '../Firebase/index'
import { useParams } from 'react-router';


const EditBlog = () => {
    const [blog, setBlog] = useState(null);
    const [progress, setProgress] = useState(0);
    const [newImg, setNewImg] = useState(null);

    let finalProgress = progress + '%';

    let history = useHistory()
    let params = useParams()
    let id = params.id

    useEffect(()=>{
        let getBlog = () =>{
            axios.get(`${baseUrl}/api/blogs/${id}/`)
            .then(response=> {setBlog(response.data)})
        }
        getBlog()
    },[id])

    let handleChange = (e) => {
        const {name, value} = e.target;
        setBlog({...blog,[name]:value})
    }

    let updateBlog = (id,e) =>{
        e.preventDefault()
        let data = blog
        console.log(data)
        axios.put(`${baseUrl}/api/blogs/${id}/update`, data)
            .then(response => {history.push(`/blog/${id}`)})
    }

    let handleImage = (e) => {
        let img = e.target.files[0]
        const uploadTask = storage.ref(`images/${img.name}`).put(img);
        uploadTask.on(
            "state_changed",
            snapshot => {
                let prog = Math.round(
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                  );
                setProgress(prog)
              },
            error => {
              console.log(error);
            },
            () => {
              storage
                .ref("images")
                .child(img.name)
                .getDownloadURL()
                .then(url => {
                    setNewImg(url)
                    setBlog(prevState =>{ return{...prevState, imgUrl:url}})
                });
            }
          );
    }

    return (
        <div className="mt-5 mb-5 container">
            <form >
                <div className="mb-3">
                    <label htmlFor="title" className="form-label">Title</label>
                    <input type="text" className="form-control" name="title" id="title" aria-describedby="title" 
                    onChange={handleChange} defaultValue={blog?.title} />
                </div>
                <div className="mb-3">
                    <img className="img-fluid" src={newImg ? newImg : blog?.imgUrl} alt={blog?.title} />
                </div>
                <div className="mb-3">
                    <label htmlFor="image" className="form-label">Replace Image</label>
                    <input type="file" className="form-control" name="imgUrl" id="image" aria-describedby="image" 
                    onChange={handleImage}  />
                    { progress > 0 && <div className="progress mt-4" style={{height: '3px'}}>
                        <div className="progress-bar bg-success" role="progressbar" style={{width: finalProgress }}
                        aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100"></div>
                    </div> }
                </div>
                <div className="mb-3">
                    <label htmlFor="author" className="form-label">Author </label>
                    <input type="text" className="form-control" name="author" id="author" aria-describedby="author" 
                    onChange={handleChange} defaultValue={blog?.author} />
                </div>
                <div className="mb-3">
                    <label htmlFor="article" className="form-label">Post</label>
                    <textarea className="form-control" name="article" aria-label="article" onChange={handleChange}
                    rows='15' defaultValue={blog?.article}></textarea>
                </div>
                <button type="button" className="btn btn-primary" onClick={(e)=> {updateBlog(blog?.id,e)}}>Submit</button>
            </form>
        </div>
    )
}

export default EditBlog
