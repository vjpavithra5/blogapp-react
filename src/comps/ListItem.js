import React from 'react'
import {Link} from 'react-router-dom'
import moment from 'moment'

const ListItem = ({blog}) => {
    return (
        <div className="list-group-item">
            <Link to={`/blog/${blog.id}`}>
                <h3 className="blog-title-link text-wrap text-break">{blog.title}</h3>
            </Link>
            <p>Posted By 
                <span className="text-info text-capitalize"> { blog.author }</span> on 
                <span className="text-info"> { moment(blog.created).format('MMMM Do YYYY') }</span>
            </p>
            <p className="text-wrap text-break">{blog.article.slice(0, 100)}...</p>
        </div>
    )
}

export default ListItem
