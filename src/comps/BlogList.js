import React, {useEffect, useState} from 'react'
import ListItem from './ListItem';
import axios from 'axios';

const BlogList = () => {
    const [blogs, setBlogs] = useState([]);

    useEffect(()=>{
        let getBlogs = () => {
            axios.get('http://127.0.0.1:8000/api/blogs/')
            .then(response => setBlogs(response.data))
        }
        getBlogs()
    },[])

    

    return (
        <div className="container mt-5 mb-5">
            <div className="list-group">
                {
                    blogs.map( (blog, index) => (
                        <ListItem key={index} blog={blog} />
                    ))
                }
            </div>
        </div>
    )
}

export default BlogList
