import React, {useState, useEffect} from 'react'
import { useParams } from 'react-router'
import { baseUrl} from '../Base';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import moment from 'moment';

const BlogPage = () => {
    let params = useParams()
    let id = params.id
    let history = useHistory()

    const [blog, setBlog] = useState(null)

    useEffect(()=>{
        let getBlog = () =>{
            axios.get(`${baseUrl}/api/blogs/${id}/`)
            .then(response=> {setBlog({isEdit:false ,...response.data})})
        }
        getBlog()
    },[id])

    let handleEdit = (id) => {
        setBlog(prevState =>{ return{...prevState,isEdit:true}})
        history.push(`/blog/${id}/edit/`)
    }

    let handleDelete = (id) => {
        axios.delete(`${baseUrl}/api/blogs/${id}/delete`)
        .then(response => {history.push('/blogs/')})
    }


    return (
        <div className="mt-5 mb-5 container">
            { !blog?.isEdit &&
            <div className="container">
                <h3>{blog?.title}</h3>
                <div className="row">
                    <div className="col-10">
                    <p>Posted By 
                        <span className="text-info text-capitalize"> { blog?.author }</span> on 
                        <span className="text-info"> { moment(blog?.created).format('MMMM Do YYYY') }</span>
                    </p>
                    </div>
                    <div className="col-1 text-end">
                        <h5 className="text-primary">
                            <i onClick={()=> {handleEdit(blog.id)}} className="bi bi-pencil-fill"></i>
                        </h5>
                    </div>
                    <div className="col-1">
                        <h5 className="text-primary">
                            <i onClick={() => {handleDelete(blog.id)}} className="bi bi-trash-fill"></i>
                        </h5>
                    </div>
                </div>
                <img  className="img-fluid" src={blog?.imgUrl} alt="blog" />
                <p className="text-wrap text-break mt-5">{blog?.article}</p>
            </div>
            }
        </div>
    )
}

export default BlogPage
