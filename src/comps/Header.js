import React from "react";
import {Link} from 'react-router-dom'

const Header = () => {
    return (
        <>
        <nav className="navbar sticky-top navbar-light bg-light shadow p-3 mb-5 bg-white rounded">
            <div className="container-fluid row">
                <div className="col-8">
                    <a className="navbar-brand" href="http://localhost:3000/">Maaya</a>
                </div>
                <div className="col-4 text-end">
                    <h1><Link to="/blogs/create"><i className="bi bi-plus"></i></Link></h1>
                </div>
            </div>
        </nav>
        </>
    )
}

export default Header